import { _decorator, Component, Node, Prefab } from "cc";
const { ccclass, property } = _decorator;

@ccclass("barrierSelect")
export class barrierSelect extends Component {

    @property({ tooltip: '关卡总数' })
    barrierTotal: number = 30;

    passTotal: number = 12;

    @property({ type: Prefab, tooltip: '已解锁关卡预制体' })
    unlockItemPrefab = null;

    @property({ type: Prefab, tooltip: '待解锁关卡预制体' })
    lockItemPrefab = null;

    @property({ type: cc.ScrollViewComponent, tooltip: "滚动视图组件" })
    scrollView = null;

    start() {
        // console.log('unlockItemPrefab: ' + this.unlockItemPrefab);

        // let unlockItem = cc.instantiate(this.unlockItemPrefab);
        // console.log('unlockItem: ' + unlockItem);

        // let src = unlockItem.getChildByName("src");
        // console.log('src: ' + src);
        // console.log('src.string: ' + src.string);
        // console.log('src.label: ' + src.getComponent(cc.Label));
        // console.log('src.LabelComponent: ' + src.getComponent(cc.LabelComponent));

        for (let i = 1; i < this.barrierTotal; i++) {
            // 已解锁关卡
            if (i < this.passTotal) {
                let unlockItem = cc.instantiate(this.unlockItemPrefab);
                // console.log(unlockItem.getChildByName("number").getComponent(cc.Label));
                unlockItem.getChildByName("src").getComponent(cc.LabelComponent).string = i;
                this.scrollView.content.addChild(unlockItem);
            }
            // 待解锁关卡
            else {
                let lockItem = cc.instantiate(this.lockItemPrefab);
                this.scrollView.content.addChild(lockItem);
            }
        }

    }

    // update (deltaTime: number) {
    //     // Your update function goes here.
    // }

}
