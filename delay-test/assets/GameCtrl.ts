const { ccclass, property } = cc._decorator;

@ccclass
export default class GameCtrl extends cc.Component {

    @property({ type: cc.Node })
    player: cc.Node = null;

    start() {
        this.player.runAction(cc.sequence(cc.fadeOut(1), cc.delayTime(1), cc.fadeIn(1)))
    }

}