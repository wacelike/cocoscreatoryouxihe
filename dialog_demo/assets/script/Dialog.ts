import { _decorator, Component, Node, SpriteComponent, Vec3, tween } from "cc";
const { ccclass, property } = _decorator;

@ccclass("Dialog")
export class Dialog extends Component {

    @property({ type: Node, tooltip: '对话框遮造层', displayOrder: 1 })
    mask: Node = null;

    @property({ type: Number, tooltip: '对话框遮造层透明度', displayOrder: 2 })
    maskOpacity: number = 200;

    @property({ type: Node, tooltip: '对话框图片', displayOrder: 3 })
    dialog: Node = null;

    showDialog(): void {

        // 对话框整体显示
        this.node.active = true;

        // 对话框图片缩放动画
        console.log(22222);
        tween(this.dialog)
            .to(3, { scale: 1.5 })
            .repeat(1) // 执行 2 次
            .start();
    }

    closeDialog(): void {
        this.node.active = false;
    }

}