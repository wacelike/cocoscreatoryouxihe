const { ccclass, property } = cc._decorator;

// 事件类型
enum EventName {
    TOUCH_START='touch-start',
    TOUCH_END='touch-end',
    BLOCK_MOVE_END = 'block_move_end',
    BLOCK_MOVE_FIX = 'block_move_fix'
}

@ccclass
export class Constant {

    public static EventName = EventName;

}