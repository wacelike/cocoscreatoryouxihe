const { ccclass, property } = cc._decorator;

@ccclass
export default class RunTimeData extends cc.Component {

    // 当前关卡
    public currentLevel: number = 1;

    // 当前汉诺塔位置摆放
    public blockPosition: Array<Array<string>> = [[], [], []];

    // 当前方块初始位置
    public orignPos: cc.Vec3 = new cc.Vec3();

    // 汉诺塔摆放位置
    public blockPosMap: { [key: number]: [string] } = {};

    private static instance: RunTimeData = null;

    public static getInstance(): RunTimeData {
        if (!this.instance) {
            this.instance = new RunTimeData();
        }
        return this.instance;
    }

}
