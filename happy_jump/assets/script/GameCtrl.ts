const { ccclass, property } = cc._decorator;

@ccclass
export default class GameCtrl extends cc.Component {

    @property({ type: cc.Node })
    playerNode: cc.Node = null;

    @property({ type: cc.Prefab })
    drumNormalPrefab: cc.Prefab = null;

    @property({ type: cc.Prefab })
    drumRedPrefab: cc.Prefab = null;

    @property({ type: cc.Node })
    drumRoot: cc.Node = null;

    static instance: GameCtrl = null;

    private drumNormalPool: cc.NodePool = null;

    private drumRedPool: cc.NodePool = null;
    // 第一个位置
    private firstDrumPos: cc.Vec2 = new cc.Vec2(-200, -400);
    // 上一个 x 轴坐标, 上一个 y 轴坐标
    private lastDrumPosX: number = null;
    private lastDrumPosY: number = null;
    // 当前第几个
    private currDrumNum: number = 1;
    // x 轴边界值
    private offsetDrumPosX: number = 210;

    onLoad(): void {
        if (GameCtrl.instance == null) {
            GameCtrl.instance = this;
        } else {
            GameCtrl.instance.destroy();
            return;
        }

        this.drumNormalPool = new cc.NodePool('drumNormal');
        this.drumRedPool = new cc.NodePool('drumRed');
    }

    start() {
        this.initGame();
    }

    // 游戏初始化
    initGame(): void {
        // 初始化鼓
        for (let i = 0; i < 6; i++) {
            this.scheduleOnce(() => { this.allocDrum(1) }, i * 0.3);
        }
        // 玩家初始位置, 并下落到第一个点
        this.scheduleOnce(() => {
            this.playerNode.setPosition(this.firstDrumPos.add(new cc.Vec2(0, 300)));
            var action = cc.moveTo(1, this.firstDrumPos.add(new cc.Vec2(0, 60))).easing(cc.easeBounceOut());
            this.playerNode.runAction(action);
        }, 0.2)
    }

    // 生成鼓
    allocDrum(type: number) {

        let drum: cc.Node = null;

        if (type === 1) {
            drum = this.drumNormalPool.get();
            if (drum == null) {
                drum = cc.instantiate(this.drumNormalPrefab);
            }
        } else if (type === 2) {
            drum = this.drumRedPool.get();
            if (drum == null) {
                drum = cc.instantiate(this.drumRedPrefab);
            }
        }

        // 第 1 个, 默认位置放置
        if (this.currDrumNum == 1) {

            drum.setPosition(this.firstDrumPos.add(new cc.Vec2(0, 1000)));
            let action = cc.moveTo(0.3, this.firstDrumPos).easing(cc.easeBounceOut());
            let end = cc.callFunc(() => {
                drum.setPosition(this.firstDrumPos);
                // 
                this.drumRoot.addChild(drum);
                console.log('2222: ' + drum.position)

                this.lastDrumPosX = drum.x;
                this.lastDrumPosY = drum.y;
                this.currDrumNum = this.currDrumNum + 1;

            })

            let seq = cc.sequence([action, end]);
            drum.runAction(seq);


        } else {
            const step = this.currDrumNum % 6;
            let currDrumX;
            let currDrumY;
            let offsetX = 100 + 50 * Math.random();
            if (step == 2 || step == 3 || step == 4) {
                currDrumX = this.lastDrumPosX + offsetX;
                currDrumX = currDrumX > this.offsetDrumPosX ? this.offsetDrumPosX : currDrumX;
            } else if (step == 5 || step == 0 || step == 1) {
                currDrumX = this.lastDrumPosX - offsetX;
                currDrumX = currDrumX < -this.offsetDrumPosX ? - this.offsetDrumPosX : currDrumX;
            }
            currDrumY = this.lastDrumPosY + (50 + 160 * Math.random());
            drum.setPosition(new cc.Vec2(currDrumX, currDrumY));

            // 
            this.drumRoot.addChild(drum);

            this.lastDrumPosX = drum.x;
            this.lastDrumPosY = drum.y;
            this.currDrumNum++;

        }



    }

    // 回收鼓
    recycleDrum(type: number, drum: cc.Node): void {
        if (type === 1) {
            this.drumNormalPool.put(drum);
        } else if (type === 2) {
            this.drumRedPool.put(drum);
        }
        console.log(this.drumNormalPool.size())
    }

}