import { _decorator, Component, Node } from "cc";
const { ccclass, property } = _decorator;

// 事件类型
enum EventName {
    GREETING = 'greeting',
    GOODBYE = 'goodbye',
    FINISHED_WALK = 'finished_walk',
    BRAKE_START = 'brake_start',
    BRAKE_STOP = 'brake_stop',
    SHOW_COIN = 'show_coin',
    GAME_START = 'game_start',
    GAME_OVER = 'game_end',
    NEW_LEVEL = 'new_level',
    SHOW_TALK = 'show_talk',
    SHOW_GUIDE = 'show_guide'
}

// 乘客状态
enum CustomerState {
    NONE,
    GREETING,
    GOODBYE
}

// 音乐及音效
enum AudioSource {
    BACKGROUND = 'background',
    CLICK = 'click',
    CARASH = 'crash',
    GET_MONEY = 'getMoney',
    IN_cAR = 'inCar',
    NEW_ORDER = 'newOrder',
    START = 'start',
    STOP = 'stop',
    TOOTING1 = 'tooting1',
    TOOTING2 = 'tooting2',
    WIN = 'win'
}

// 小车类型
enum CarGroup {
    NORMAL = 1 << 0,
    MAIN_CAR = 1 << 1,
    OTHER_CAR = 1 << 2
}

enum UIPage {
    mainUI = 'mainUI',
    gameUI = 'gameUI',
    resultUI = 'resultUI'
}

@ccclass("Constants")
export class Constants {

    public static EventName = EventName;

    public static CustomerState = CustomerState;

    public static AudioSource = AudioSource;

    public static CarGroup = CarGroup;

    public static talkTable = [
        '麻溜的, 师傅 \n 我要赶飞机',
        'Are you ok ?',
        '天气不错'
    ]

    public static UIPage = UIPage;

}