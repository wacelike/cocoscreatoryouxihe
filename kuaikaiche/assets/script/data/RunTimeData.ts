import { _decorator, Component, Node } from "cc";
const { ccclass, property } = _decorator;

@ccclass("RunTimeData")
export class RunTimeData {

    public currProcess: number = 0;
    public maxProcess: number = 2;
    public isOverOrder: boolean = true;

    private static instance: RunTimeData = null;

    public static getInstance(): RunTimeData {
        if (!this.instance) {
            this.instance = new RunTimeData();
        }
        return this.instance;
    }

}
