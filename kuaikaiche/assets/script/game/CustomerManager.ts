import { _decorator, Component, Node, Vec3, AnimationComponent } from "cc";
import { Constants } from "../data/Constants";
import { CustomerEventListener } from "../data/CustomerEventListener";
const { ccclass, property } = _decorator;

const EventName = Constants.EventName;
const tempVec = new Vec3();

@ccclass("CustomerManager")
export class CustomerManager extends Component {

    @property({ type: [Node], tooltip: '乘客' })
    customer: Node[] = [];

    @property({ tooltip: '乘客行走时间(秒)' })
    walkTime: number = 2;

    // 当前顾客
    private currentCustomer: Node = null;

    // 顾客起始点、结束点
    private startPos: Vec3 = new Vec3();
    private endPos: Vec3 = new Vec3();

    //
    private isInOrder: boolean = false;
    private deltaTime: number = 0;
    private customerState = Constants.CustomerState.NONE;

    start(): void {
        CustomerEventListener.on(EventName.GREETING, this.greetingCustomer, this);
        CustomerEventListener.on(EventName.GOODBYE, this.goodbyeCustomer, this);
    }

    update(dt: number): void {
        if (this.isInOrder) {
            this.deltaTime += dt;
            if (this.deltaTime > this.walkTime) {
                this.deltaTime = 0;
                this.isInOrder = false;
                this.currentCustomer.active = false;
                // 乘客送走, 直接清空
                if (this.customerState === Constants.CustomerState.GOODBYE) {
                    this.currentCustomer = null;
                }
                // 时间到了, 小车继续走
                CustomerEventListener.dispatchEvent(EventName.FINISHED_WALK);
            } else {
                Vec3.lerp(tempVec, this.startPos, this.endPos, this.deltaTime / this.walkTime);
                this.currentCustomer.setWorldPosition(tempVec);
            }
        }
    }

    private greetingCustomer(...args: any[]): void {

        this.customerState = Constants.CustomerState.GREETING;
        this.isInOrder = true;

        // 乘客类型
        let customerIndex = Math.floor(Math.random() * this.customer.length);
        if (customerIndex === this.customer.length) {
            customerIndex = this.customer.length - 1;
        }
        this.currentCustomer = this.customer[customerIndex];
        if (!this.currentCustomer) {
            return;
        }



        // 接收参数, 计算乘客出发、结束点
        const carPosition = args[0];
        const customerDirection = args[1];
        Vec3.multiplyScalar(this.startPos, customerDirection, 1.4);
        this.startPos.add(carPosition);
        Vec3.multiplyScalar(this.endPos, customerDirection, 0.5);
        this.endPos.add(carPosition);
        this.currentCustomer.setWorldPosition(this.startPos);
        this.currentCustomer.active = true;

        // 乘客面部朝向
        const x = customerDirection.x;
        const z = customerDirection.z;
        if (x !== 0) {
            if (x > 0) {
                this.currentCustomer.eulerAngles = new Vec3(0, 270, 0);
            } else {
                this.currentCustomer.eulerAngles = new Vec3(0, 90, 0);
            }
        } else if (z !== 0) {
            if (z > 0) {
                this.currentCustomer.eulerAngles = new Vec3(0, 0, 0);
            } else {
                this.currentCustomer.eulerAngles = new Vec3(0, 180, 0);
            }
        }

        // 播放动画
        const animComp = this.currentCustomer.getComponent(AnimationComponent);
        animComp.play('walk');

        let customerId = customerIndex + 1;
        CustomerEventListener.dispatchEvent(Constants.EventName.SHOW_TALK, customerId);

    }

    private goodbyeCustomer(...args: any[]): void {

        this.customerState = Constants.CustomerState.GOODBYE;
        this.isInOrder = true;

        // 接收参数, 计算乘客出发、结束点
        const carPosition = args[0];
        const customerDirection = args[1];
        Vec3.multiplyScalar(this.startPos, customerDirection, 0.5);
        this.startPos.add(carPosition);
        Vec3.multiplyScalar(this.endPos, customerDirection, 1.4);
        this.endPos.add(carPosition);
        this.currentCustomer.setWorldPosition(this.startPos);
        this.currentCustomer.active = true;

        // 乘客面部朝向
        const x = customerDirection.x;
        const z = customerDirection.z;
        if (x !== 0) {
            if (x > 0) {
                this.currentCustomer.eulerAngles = new Vec3(0, 90, 0);
            } else {
                this.currentCustomer.eulerAngles = new Vec3(0, 270, 0);
            }
        } else if (z !== 0) {
            if (z > 0) {
                this.currentCustomer.eulerAngles = new Vec3(0, 180, 0);
            } else {
                this.currentCustomer.eulerAngles = new Vec3(0, 0, 0);
            }
        }

        // 播放动画
        const animComp = this.currentCustomer.getComponent(AnimationComponent);
        animComp.play('walk');

    }

}
