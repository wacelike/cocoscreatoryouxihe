import { _decorator, Component, Node, Vec3, Enum, macro } from "cc";
const { ccclass, property } = _decorator;

enum ROAD_POINT_TYPE {
    NORMAL = 1,
    START,
    GREETING,
    GOODBYE,
    END,
    STOP_MOVING,
    AI_START
}
Enum(ROAD_POINT_TYPE);

enum ROAD_MOVE_TYPE {
    LINE = 1,
    BEND /** 弯道 */
}
Enum(ROAD_MOVE_TYPE);


@ccclass("RoadPoint")
export class RoadPoint extends Component {

    // 把枚举开放出来
    public static RoadPointType = ROAD_POINT_TYPE;
    public static RoadMoveType = ROAD_MOVE_TYPE;

    @property({ type: ROAD_POINT_TYPE, tooltip: '节点类型', displayOrder: 1 })
    type: ROAD_POINT_TYPE = ROAD_POINT_TYPE.NORMAL;

    @property({
        type: Node, tooltip: '小车下一位置节点', displayOrder: 2,
        visible: function (this: RoadPoint) {
            return this.type !== ROAD_POINT_TYPE.STOP_MOVING;
        }
    })
    nextStation: Node = null;

    @property({
        type: ROAD_MOVE_TYPE, tooltip: '马路类型: 直道、转弯', displayOrder: 3,
        visible: function (this: RoadPoint) {
            return this.type !== ROAD_POINT_TYPE.STOP_MOVING;
        }
    })
    moveType: ROAD_MOVE_TYPE = ROAD_MOVE_TYPE.LINE;

    @property({
        tooltip: '小车转弯方向：true 顺时针, false 逆时针', displayOrder: 4,
        visible: function (this: RoadPoint) {
            return this.moveType === ROAD_MOVE_TYPE.BEND;
        }
    })
    clockWise: boolean = true;

    @property({
        tooltip: '接送客方向', displayOrder: 5,
        visible: function (this: RoadPoint) {
            return this.type === ROAD_POINT_TYPE.GREETING || this.type === ROAD_POINT_TYPE.GOODBYE;
        }
    })
    direction: Vec3 = new Vec3(1, 0, 0);

    @property({
        tooltip: 'AI 延迟生成小车时间(秒)', displayOrder: 6,
        visible: function (this: RoadPoint) {
            return this.type === ROAD_POINT_TYPE.AI_START;
        }
    })
    delayTime: number = 3;

    @property({
        tooltip: 'AI 生成小车间隔(秒)', displayOrder: 7,
        visible: function (this: RoadPoint) {
            return this.type === ROAD_POINT_TYPE.AI_START;
        }
    })
    interval: number = 3;

    @property({
        tooltip: 'AI 生成小车速度', displayOrder: 8,
        visible: function (this: RoadPoint) {
            return this.type === ROAD_POINT_TYPE.AI_START;
        }
    })
    speed: number = 0.05;

    @property({
        tooltip: 'AI 生成小车类型', displayOrder: 9,
        visible: function (this: RoadPoint) {
            return this.type === ROAD_POINT_TYPE.AI_START;
        }
    })
    cars: string = '';

    private arriveCars: string[] = [];
    private cd: Function = null;

    public start(): void {
        this.arriveCars = this.cars.split(',');
    }

    public startSchedule(cd: Function): void {
        if (this.type !== ROAD_POINT_TYPE.AI_START) {
            return;
        }
        this.stopSchedule();
        this.cd = cd;
        console.log('startSchedule............... this.cd: ' + cd)
        this.scheduleOnce(this.startDelay, this.delayTime);
    }

    public stopSchedule(): void {
        this.unschedule(this.startDelay);
        this.unschedule(this.scheduleCD);
    }

    private startDelay() {
        // console.log('startDelay...............')
        this.scheduleCD();
        this.schedule(this.scheduleCD, this.interval, macro.REPEAT_FOREVER);
    }

    private scheduleCD() {

        let index = Math.floor(Math.random() * this.arriveCars.length);
        if (index === this.arriveCars.length) {
            index -= 1;
        }

        // console.log('scheduleCD............... this.arriveCars[index]: ' + this.arriveCars[index]);

        if (this.cd) {
            this.cd(this, this.arriveCars[index]);
        }

    }

}
