import { _decorator, Component, Node, LabelComponent, SpriteComponent, SpriteFrame, loader } from "cc";
import { RunTimeData } from "../data/RunTimeData";
import { CustomerEventListener } from "../data/CustomerEventListener";
import { Constants } from "../data/Constants";
const { ccclass, property } = _decorator;

@ccclass("GameUI")
export class GameUI extends Component {

    @property({
        type: LabelComponent,
        displayOrder: 1
    })
    targetLevel: LabelComponent = null;

    @property({
        type: LabelComponent,
        displayOrder: 2
    })
    srcLevel: LabelComponent = null;

    @property({
        type: SpriteComponent,
        displayOrder: 3
    })
    targetSp: SpriteComponent = null;

    @property({
        type: SpriteComponent,
        displayOrder: 4
    })
    srcSp: SpriteComponent = null;

    @property({
        type: SpriteFrame,
        displayOrder: 5
    })
    levelFinished: SpriteFrame = null;

    @property({
        type: SpriteFrame,
        displayOrder: 5
    })
    levelUnFinished: SpriteFrame = null;


    @property({
        type: [SpriteComponent],
        displayOrder: 7
    })
    process: SpriteComponent[] = [];

    @property({
        type: SpriteFrame,
        displayOrder: 7
    })
    process1: SpriteFrame = null;

    @property({
        type: SpriteFrame,
        displayOrder: 10
    })
    process2: SpriteFrame = null;

    @property({
        type: SpriteFrame,
        displayOrder: 11
    })
    process3: SpriteFrame = null;

    @property({ type: Node, displayOrder: 100, tooltip: '对话框' })
    talkDialog: Node = null;

    @property({ type: SpriteComponent, displayOrder: 101, tooltip: '对话框-人物头像' })
    talkDialogAvator: SpriteComponent = null;

    @property({ type: LabelComponent, displayOrder: 102, tooltip: '对话框-人物对白' })
    talkDialogContent: LabelComponent = null;

    private runTimeData: RunTimeData = null;

    public show(): void {
        this.runTimeData = RunTimeData.getInstance();
        this.refreshUI();

        CustomerEventListener.on(Constants.EventName.GREETING, this.greeting, this);
        CustomerEventListener.on(Constants.EventName.GOODBYE, this.goodBye, this);
        CustomerEventListener.on(Constants.EventName.SHOW_TALK, this.talk, this);
        CustomerEventListener.on(Constants.EventName.SHOW_GUIDE, this.showGuide, this);
    }

    public hide(): void {

    }

    private refreshUI(): void {
        for (let i = 0; i < this.process.length; i++) {
            const process: SpriteComponent = this.process[i];
            if (i >= this.runTimeData.maxProcess) {
                process.node.active = false;
            } else {
                process.node.active = true;
                process.spriteFrame = this.process1;
            }
        }

        this.srcLevel.string = '1';
        this.targetLevel.string = '2';
        this.srcSp.spriteFrame = this.levelFinished;
        this.targetSp.spriteFrame = this.levelUnFinished;
    }

    /**
     * 接客
     */
    private greeting(): void {
        const curr = this.runTimeData.currProcess;
        console.log('greeting, curr: ' + curr);
        this.process[curr].spriteFrame = this.process2;
    }

    /**
     * 送客
     */
    private goodBye(): void {
        const curr = this.runTimeData.currProcess;
        console.log('goodBye, curr: ' + curr);
        this.process[curr].spriteFrame = this.process3;
    }

    /**
     * 对话
     */
    private talk(customerId: string): void {

        // 获取对话内容
        const table = Constants.talkTable;
        let index = Math.floor(Math.random() * table.length);
        if (index === table.length) {
            index--;
        }
        const talkContent = table[index];

        // 对话内容赋值
        this.talkDialogContent.string = talkContent;

        // 获取用户头像及展示
        const avatorPath = `texture/head/head${customerId}/spriteFrame`;
        loader.loadRes(avatorPath, SpriteFrame, (err: any, sp: SpriteFrame) => {
            if (err) {
                console.log('error: ' + err);
                return;
            }
            this.talkDialogAvator.spriteFrame = sp;
        });

        // 对话框显示延迟显示, 确保图片已显示
        this.scheduleOnce(() => {
            this.talkDialog.active = true;
        }, 0.3);

        // 对话框显示延迟消失, 停留显示
        this.scheduleOnce(() => {
            this.talkDialog.active = false;
            this.talkDialogAvator.spriteFrame = null;
        }, 3);

    }

    /**
     * 展示引导
     */
    private showGuide(): void {

    }

}
