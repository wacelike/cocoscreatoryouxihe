import { _decorator, Component, Node } from "cc";
import { CustomerEventListener } from "../data/CustomerEventListener";
import { Constants } from "../data/Constants";
const { ccclass, property } = _decorator;

@ccclass("MainUI")
export class MainUI extends Component {

    clickStart(): void {
        console.log('MainUI startGame dispatchEvent ..................... ')
        CustomerEventListener.dispatchEvent(Constants.EventName.GAME_START);
    }

}