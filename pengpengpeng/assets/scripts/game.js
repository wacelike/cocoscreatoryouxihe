// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        ballon_prefab: {
            type: cc.Prefab,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.scheduleOnce(this.gen_balloon().bind(this), 0.5);
    },

    go_home() {
        cc.director.loadScene("home")
    },

    gen_balloon() {

        // 速度
        var speed = Math.random() * 400 + 400;
        // 类型
        var type = Math.random() * 7;
        type = Math.ceil(type);

        // 实例化气球
        var balloon = cc.instantiate(this.ballon_prefab);
        balloon.active = true;
        this.node.addChild(balloon);

        // 设定初始化坐标
        var xpos = (Math.random() - 0.5) * 270;
        var ypos = Math.random() * -300 - 100;
        balloon.x = xpos;
        balloon.y = ypos;

        var ballon_component = balloon.getComponent("balloon");
        ballon_component.gen_balloon(type, speed);

        // 随机事件后, 生成新的气球
        var time = Math.random() * 1 + 1.5;
        this.scheduleOnce(this.gen_balloon.bind(this), time);
        console.log('random 气球');
    }

    // update (dt) {},
});
