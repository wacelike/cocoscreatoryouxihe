// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        bg_music: {
            type: cc.AudioClip,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        let logo, s_max, s_min, seq, rf;

        // 模块 1 动起来
        logo = cc.find('UI_ROOT/boom_1');
        s_max = cc.scaleTo(1.5, 1.1);
        s_min = cc.scaleTo(1.5, 0.9);
        seq = cc.sequence([s_max, s_min]);
        rf = cc.repeatForever(seq);
        logo.runAction(rf);

        // 模块 2 动起来
        logo = cc.find('UI_ROOT/boom_2');
        s_max = cc.scaleTo(2.0, 1.6);
        s_min = cc.scaleTo(2.0, 0.9);
        seq = cc.sequence([s_max, s_min]);
        rf = cc.repeatForever(seq);
        logo.runAction(rf);

        // 模块 3 动起来
        logo = cc.find('UI_ROOT/boom_3');
        s_max = cc.scaleTo(2.5, 1.3);
        s_min = cc.scaleTo(2.5, 0.9);
        seq = cc.sequence([s_min, s_max]);
        rf = cc.repeatForever(seq);
        logo.runAction(rf);

        // 开始按钮转起来
        var start_btn = cc.find('UI_ROOT/start_btn');
        var rot = cc.rotateBy(1.0, 360).easing(cc.easeCubicActionOut());
        var rf1 = cc.repeatForever(rot);
        start_btn.runAction(rf1);

        // 背景音乐播放
        cc.audioEngine.stopAll();
        cc.audioEngine.playMusic(this.bg_music, true)

    },

    start() {

    },

    go_play() {
        cc.director.loadScene("game")
    }

    // update (dt) {},
});
