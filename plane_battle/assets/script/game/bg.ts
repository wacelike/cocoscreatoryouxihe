const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property
    public speed: number = -400;

    private bg_red: cc.Node = null;
    private bg_green: cc.Node = null;
    private bottom_bg: cc.Node = null;

    private bgHeight: number = 0;

    // onLoad () {}

    start() {
        this.rollingBackground();
    }


    // 背景图片滚动
    rollingBackground(): void {

        this.bg_red = this.node.getChildByName("bg-a");
        this.bg_green = this.node.getChildByName("bg-b");
        this.bottom_bg = this.bg_red;

        this.bgHeight = this.bottom_bg.height;

        // this.node.zIndex = -2000;
    }

    update(dt): void {

        var s = dt * this.speed;
        this.bg_red.y += s;
        this.bg_green.y += s;

        if (this.bottom_bg.y <= -this.bgHeight) { // 地图切换
            if (this.bottom_bg == this.bg_red) {
                this.bg_red.y = this.bg_green.y + this.bgHeight;
                this.bottom_bg = this.bg_green;
            }
            else {
                this.bg_green.y = this.bg_red.y + this.bgHeight;
                this.bottom_bg = this.bg_red;
            }
        }

    }

}
