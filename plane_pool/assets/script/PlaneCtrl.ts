import GameCtrl from "./GameCtrl";
import BulletCtrl from "./BulletCtrl";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PlaneCtrl extends cc.Component {

    @property({ type: Number })
    bulletCoolTime: number = 0.02;

    private bulletStartTime: number = 0;

    start() {
    }

    movePlane(e: cc.Touch): void {
        this.node.x += e.getDelta().x;
        this.node.y += e.getDelta().y;
    }

    shootBullet(): void {

        let bulletNode = GameCtrl.instance.allocBullet();
        let BulletCtrl: BulletCtrl = bulletNode.getComponent('BulletCtrl');
        BulletCtrl.initBulletConfig();

        // 飞机在哪, 子弹在哪
        var planePos: cc.Vec2 = this.node.getPosition();
        planePos.y += 66;
        bulletNode.setPosition(planePos);

        // console.log('***********发射子弹')
    }

    update(dt: number) {
        if (GameCtrl.instance.isPause) {
            return;
        }

        // 大于等于冷却时间, 重置冷却时间并发射子弹
        if (this.bulletStartTime >= this.bulletCoolTime) {
            this.bulletStartTime = 0;
            this.shootBullet();
            // console.log('冷却ok, 发射子弹')
        }
        this.bulletStartTime += dt;
    }

}
